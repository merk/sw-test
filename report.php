<?php

abstract class report
{
    //данные для отчета
    public $data = [];
    //выборка и подготовка данных
    abstract public function createReport();
    //вывод данных
    public function printResult(string $template)
    {
        switch ($template) {
            case 'csv' :

	            /**
	             * Можно использовать ещё пакеты из композера или
	             * сохранить в файл использая нативнаые функции для php.
	             */

	            header('Content-Type: application/csv');
	            header('Content-Disposition: attachment; filename=result.csv');
	            header('Pragma: no-cache');

	           echo "Count\tSum\tDate\t\n";

	            foreach ($this->data as $row) {
	            	echo $row['ct'] . "\t"
		                 . round($row['sm'], 2) . "\t"
		                 . $row['mn'] . "\n";
	            }

                break;
            default:
                break;
        }
    }
}
